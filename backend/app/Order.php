<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id', 'address', 'total', 'payment_mode'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public static function place(array $data)
    {
        $order = [
            'total' => $data['total'],
            'address' => $data['address'],
            'user_id'=> auth()->user()->id,
            'payment_mode' => $data['payment_mode']
        ];

        $order = self::create($order);
        array_map(function($item) use ($order){
            $order->items()->create($item);
            auth()->user()->carts()->delete();
        }, $data['items']);
        return $order;
    }

    public function dispatch()
    {
        $this->dispatched = true;
        $this->save();
    }
}
