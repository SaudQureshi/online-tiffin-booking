<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = ['item_id', 'order_id', 'quantity', 'sub_total'];

    public $timestamps = false;

    protected $with = ['item'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function item()
    {
        return $this->belongsTo(Item::class,'item_id');
    }
}
