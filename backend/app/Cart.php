<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $with = ['item'];
    protected $fillable = [
        'user_id', 'item_id', 'quantity', 'sub_total'
    ];

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
