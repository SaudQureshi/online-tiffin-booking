<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function customers()
    {
        $customers = User::latest()->customers()->paginate(20);
        return view('customers', compact('customers'));
    }

    public function deleteCustomer(User $user)
    {
        $user->delete();
        session()->flash('success', "{$user->name} deleted");
        return back();
    }
}
