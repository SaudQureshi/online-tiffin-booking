<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ]);
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        return $user;
    }

    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|exists:users',
            'password' => ['required', function($attribute, $value, $fail) use($request){
                $user = User::whereEmail($request->email)->first();
                if($user){
                    if(!Hash::check($request->password, $user->password)){
                        return $fail("Password Incorrect");
                    }
                }
            }]
        ]);

        $user = User::whereEmail($data['email'])->first();

        return $user;
    }
}
