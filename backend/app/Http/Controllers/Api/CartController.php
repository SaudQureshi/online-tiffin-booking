<?php

namespace App\Http\Controllers\Api;

use App\Cart;
use App\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return auth()->user()->carts()->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'item_id' => 'required|exists:items,id',
            'quantity' => 'nullable|numeric'
        ]);
        $item = Item::find($data['item_id'])->toArray();
        $item['item_id'] = $item['id'];
        $item['user_id'] = auth()->user()->id;
        $item['quantity'] = $data['quantity'] ?? 1;
        $item['sub_total'] = $item['price']* $item['quantity'];

        Cart::create($item);
        return ['message' => 'Item Added To Cart'];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        $data = $request->validate([
            'quantity' => 'required|numeric',
        ]);
        $data['sub_total'] = $data['quantity'] * $cart->item->price;
        $cart->update($data);

        return ['message' => 'updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        $cart->delete();

        return ['message' => 'Removed'];
    }
}
