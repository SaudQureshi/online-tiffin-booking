<?php

namespace App\Http\Controllers\Api;

use App\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemController extends Controller
{
    public function index(Request $request)
    {
        $items = Item::skip($request->skip ?? 0)->take(20)->get();

        return $items;
    }
}
