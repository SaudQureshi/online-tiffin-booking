<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::with('user')->latest()->paginate(20);
        return view('orders', compact('orders'));
    }

    public function markDispatched(Order $order)
    {
        $order->dispatch();
        session()->flash('success', "Order for {$order->user->name} disptached");
        return back();
    }
}
