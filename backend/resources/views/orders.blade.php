@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            All Orders
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            From
                        </th>
                        <th>
                            Total
                        </th>
                        <th>Status</th>
                        <th>
                            Ordered At
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                    <tr>
                        <td>
                            {{ $loop->iteration }}
                        </td>
                        <td>
                            {{ $order->user->name }}
                        </td>
                        <td>
                            {{ $order->total }} Rs.
                        </td>
                        <td>
                            @if ($order->dispatched)
                            <span class="badge badge-success p-2">
                                dispatched
                            </span>
                            @else
                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#dispatch{{ $order->id }}">
                                dispatch
                            </button>

                            <!-- The Modal -->
                            <div class="modal" id="dispatch{{ $order->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Dispatch order</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <form action="{{ route('orders.dispatch', $order->id) }}" method="post">
                                                @csrf
                                                @method('put')
                                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-success btn-sm">Dispatch</button>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            @endif
                        </td>
                        <td>
                            {{ $order->created_at->format('d M Y') }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
