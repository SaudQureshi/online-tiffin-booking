@extends('layouts.app')

@section('content')
<div class="container">
    <div class="w-75 mx-auto">
        <div class="card">
            <div class="card-header">
                Add new item
            </div>
            <div class="card-body">
                <form action="{{ route('items.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" placeholder="Enter item name here" name="name"
                            value="{{ old('name') }}">
                        @if ($errors->has('name'))
                        <strong class="text-danger">{{ $errors->first('name') }}</strong>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        <div class="input-group">
                            <label for="image"></label>
                            <span class="input-group-btn">
                                <a id="lfm" data-input="thumbnail" data-preview="holder"
                                    class="btn btn-primary text-white">
                                    <i class="fa fa-picture-o"></i> Choose
                                </a>
                            </span>
                            <input id="thumbnail" class="form-control" type="text" name="image"
                                placeholder="Select an image for item" value="{{ old('image') }}">
                        </div>
                        <img id="holder" style="margin-top:15px;max-height:100px;" src="{{ old('image') }}">
                        @if ($errors->has('image'))
                        <strong class="text-danger">{{ $errors->first('image') }}</strong>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control" name="description"
                            placeholder="Enter item description">{{ old('description') }}</textarea>
                        @if ($errors->has('description'))
                        <strong class="text-danger">{{ $errors->first('description') }}</strong>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="number" placeholder="Enter price here" name="price" value="{{ old('price') }}"
                            class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-sm btn-primary btn-block">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
    $('#lfm').filemanager('image');
</script>
@endsection