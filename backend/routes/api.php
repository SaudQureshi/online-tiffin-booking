<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function() {
    Route::get('items', 'Api\ItemController@index');
    Route::get('carts', 'Api\CartController@index');
    Route::post('carts', 'Api\CartController@store');
    Route::put('carts/{cart}', 'Api\CartController@update');
    Route::delete('carts/{cart}', 'Api\CartController@destroy');
    Route::apiResource('orders', 'Api\OrderController');
});


Route::post('/auth/register', 'Api\AuthController@register');
Route::post('/auth/login', 'Api\AuthController@login');