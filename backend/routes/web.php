<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth', 'admin']], function(){
    Route::get('/customers', 'HomeController@customers');
    Route::delete('/customers/{user}', 'HomeController@deleteCustomer')->name('customers.destroy');
    Route::get('/orders', 'OrderController@index');
    Route::put('/orders/{order}', 'OrderController@markDispatched')->name('orders.dispatch');
    Route::resource('items','ItemController')->except('show');
});