import axios from 'axios';

let instance = axios.create();
instance.defaults.baseURL = 'http://192.168.0.105:8000';
instance.defaults.timeout = 20000;
instance.defaults.headers['Content-Type'] = 'application/json';
instance.defaults.headers['Accept'] = 'application/json';

export default instance;