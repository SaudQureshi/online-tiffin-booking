import React from 'react';
import {View} from 'react-native';

export default Header = (props) => (
    <View style={{ width: '100%', height: '10%', justifyContent: 'center', alignItems: 'center', backgroundColor:'white', elevation: 5 }}>
        {props.children}
    </View>
)