import React, { Component } from "react";
import {View, Text, Image, TouchableNativeFeedback} from'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import axios from '../utils/axios';

export default Card = (props) => (
    <>
        <View style={{ height: 250, backgroundColor: 'white', margin: 10, elevation: 5, borderRadius: 5 }}>
        <Image source={{ uri: axios.defaults.baseURL+props.item.image }} style={{ flex:2, width: '100%',borderTopLeftRadius: 5, borderTopRightRadius: 5 }}/>
            <View style={{ padding: 10, flex:1 }}>
                <View style={{ flex:1, flexDirection:'column',justifyContent:'space-between' }}>
                    <View style={{ flex:1, flexDirection:'row', justifyContent: 'space-between', width: '100%'}}>
                        <Text style={{ fontSize: 20 }}>{props.item.name}</Text>
                        <Text style={{ fontSize: 15 }}>{props.item.price} Rs.</Text>
                    </View>
                </View>
            </View>
            <TouchableNativeFeedback onPress={props.showItem}>
                <View style={{ backgroundColor:'skyblue', borderBottomLeftRadius: 5, borderBottomRightRadius: 5, padding: 5,position: 'absolute',bottom: 0,right: 0, width: '100%' }}>
                    <Text style={{ color: 'white', textAlign: 'center', paddingVertical: 2 }}><Icon name="utensils" size={10}/> Order Now</Text>
                </View>
            </TouchableNativeFeedback>
        </View>
    </>
)