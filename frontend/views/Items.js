import React, { Component } from "react";
import {View, FlatList, ToastAndroid}from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Card from '../components/card';
import axios from '../utils/axios';

export default class Item extends Component{

    state = {
        items : [],
        refreshing: false
    }
    componentDidMount(){
        this._fetchItems();
    }
    _fetchItems = async() => {
        try {
            let api_token = await AsyncStorage.getItem('api_token');
            let response = await axios({
                method: 'GET',
                url: '/api/items',
                headers:{
                    'Authorization': 'Bearer '+api_token
                }
            })
            
            this.setState({items: response.data});
            
        } catch (error) {
            ToastAndroid.showWithGravity('Error Occured', ToastAndroid.SHORT, ToastAndroid.BOTTOM)
        }
    }

    _refresh = async () => {
        await this.setState({refreshing: true})
        await this._fetchItems();
        this.setState({refreshing: false})
    }

    render(){
        return(
            <View>
                <FlatList onRefresh={this._refresh} refreshing={this.state.refreshing} data={this.state.items} keyExtractor={(item, index) => item.id.toString()} renderItem={({item}) => (
                    <Card item={item} showItem={() => this.props.navigation.navigate('Item',{item})} contentContainerStyle={{ backgroundColor: 'lightgrey' }}/>
                )
                }/>
            </View>
        )
    }
}
