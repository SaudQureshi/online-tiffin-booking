import React, { Component } from "react";
import { View, Image, Text, ToastAndroid, FlatList } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from '../utils/axios';

export default class Orders extends Component{

    state = {
        orders: [],
        refreshing: false
    }

    componentDidMount(){
        this._fetchOrders();
    }

    _fetchOrders = async() => {
        try {
            let api_token = await AsyncStorage.getItem('api_token');

            let response = await axios({
                method:'get',
                url: '/api/orders',
                headers:{
                    'Authorization': 'Bearer '+api_token
                }
            })
            this.setState({orders: response.data});
        } catch (error) {
            if(error.hasOwnProperty('response')){
                return ToastAndroid.showWithGravity(error.response.data.message, ToastAndroid.LONG, ToastAndroid.BOTTOM);
            }
            return ToastAndroid.showWithGravity(error.message, ToastAndroid.SHORT, ToastAndroid.BOTTOM);
        }
    }

    _refresh = async () => {
        await this.setState({refreshing: true})
        await this._fetchOrders();
        this.setState({refreshing: false})
    }
    
    render(){
        return(
            <View>
                <FlatList data={this.state.orders} onRefresh={this._refresh} refreshing={this.state.refreshing} keyExtractor={(item) => item.id.toString()} renderItem = {({item}) => (
                    <View style={{ height: 'auto', backgroundColor: 'white', margin: 10, elevation: 5, borderRadius: 5 }}>
                        <View style={{ padding: 10, flex:1 }}>
                       <View style={{ flex:1, flexDirection:'column',justifyContent:'space-between' }}>
                                <View style={{ flex:1, flexDirection:'row', justifyContent: 'space-between', width: '100%'}}>
                                    <Text style={{ fontSize: 20 }}>Address: {item.address}</Text>
                                    <Text style={{ fontSize: 15 }}>Total: {item.total} Rs.</Text>
                                </View>
                                    <Text style={{ paddingTop: 5 }}>Status: {item.dispatched ? 'Dispatched': 'Not dispatched yet'}</Text>
                            </View>
                        </View>
                    </View>
                )}/>
            </View>
        )
    }
}