import React, { Component } from "react";
import { View, Text, Image, Button, ToastAndroid } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from '../utils/axios';
import Spinner from 'react-native-number-spinner';

export default class Item extends Component{
    state = {
        quantity: 1
    }
    _addToCart = async (item_id) => {
        try {
            let api_token = await AsyncStorage.getItem('api_token');
            let response = await axios({
                method: 'POST',
                url: '/api/carts',
                data:{
                    item_id,
                    quantity: this.state.quantity
                },
                headers:{
                    'Authorization': 'Bearer '+api_token
                }
            })
            ToastAndroid.showWithGravity(response.data.message, ToastAndroid.LONG, ToastAndroid.BOTTOM)
        } catch (error) {
            if(error.hasOwnProperty('response')){
                ToastAndroid.showWithGravity(error.response.data.message, ToastAndroid.LONG, ToastAndroid.BOTTOM)
            }
        }
    }
    _setQuantity = (quantity) => {
        this.setState({quantity})
    }
    render(){
        const {navigation} = this.props;
        return(
            <View style={{ flex:1 }}>
                <Image source={{ uri: axios.defaults.baseURL+navigation.getParam('item').image }} style={{ height: 200, width: '100%' }}/>
                    <View style={{ marginTop: 20, marginLeft: 10 }}>
                        <Text style={{ fontSize: 25 }}>{navigation.getParam('item').name}</Text>
                        <View style={{ marginVertical: 10 }}>
                            <Text style={{ fontSize: 17 }}>Description</Text>
                            <Text style={{ padding: 5 }}>{navigation.getParam('item').description}</Text>
                        </View>
                        <Text>Price: {navigation.getParam('item').price} Rs Only /-</Text>
                        <View style={{ flex: 1, width: '100%', flexDirection:'column', alignItems: 'flex-end', height: 20, padding:20 }}>
                            <View style={{ height: 20 }}>
                                <Spinner max={10}
                                    min={1}
                                    value={this.state.quantity}
                                    color="#f60"
                                    numColor="#f60"
                                    onNumChange={this._setQuantity}
                                />
                            </View>
                        </View>

                    </View>
                    <View style={{ marginTop: 30, paddingHorizontal: 20 }}>
                        <Button title="Add to cart" onPress={() => { this._addToCart(navigation.getParam('item').id)}}/>
                    </View>
            </View>
        )
    }
}