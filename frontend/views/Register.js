import React,{ Component } from 'react';
import { Keyboard, ToastAndroid, View, Text, TextInput, TouchableOpacity, TouchableNativeFeedback } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from '../utils/axios';

export default class Register extends Component{
    state = {
        keyboardState: 'closed',
        name:'',
        email:'',
        password:'',
        password_confirmation: ''
    }

    componentDidMount(){
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }
    componentWillUnmount(){
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    _keyboardDidShow = () => {
        this.setState({
            keyboardState: 'opened'
        });
    }
    
    _keyboardDidHide = () => {
        this.setState({
            keyboardState: 'closed'
        });
    }
    _register = async () => {
            try {
                let response = await axios({
                    method:'POST',
                    url:'/api/auth/register',
                    data: {
                        name: this.state.name,
                        email: this.state.email,
                        password: this.state.password,
                        password_confirmation: this.state.password_confirmation,
                    }
                })
                await AsyncStorage.setItem('api_token', response.data.api_token)
                this.props.navigation.navigate('TabNavigatorContainer')

            } catch (error) {
                if(error.hasOwnProperty('response')){
                    this.errors = error.response.data;
                    return ToastAndroid.showWithGravity('Cannot Login', ToastAndroid.SHORT, ToastAndroid.CENTER)
                }
                ToastAndroid.showWithGravity('Cannot Login', ToastAndroid.SHORT, ToastAndroid.CENTER)   
            }
    }
    _renderDock = () =>{
          if(this.state.keyboardState =='closed'){
              return (
                <View style={{ position: 'absolute',bottom: 0,padding:10,width: '100%', backgroundColor:'orange' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                        <View>
                            <Text style={{ textAlign: 'center', color: 'white' }}>Login</Text>
                        </View>
                    </TouchableOpacity>
                </View>
              )
          }
          return null;
    }
    _setName = (value) => {
        this.setState({name: value})
    }
    _setEmail = (value) => {
        this.setState({email: value})
    }
    _setPassword = (value) => {
        this.setState({password: value})
    }
    _setConfirmPassword = (value) => {
          this.setState({password_confirmation: value})
    }
    render(){
        return(
            <View style={{ flex:1, flexDirection: 'column' }}>
                <View 
                style={{ flex:1, flexDirection: 'row', justifyContent: 'center', marginTop: '30%'}}>
                    <View style={{ flex:1, paddingHorizontal: 10 }}>
                        <Text style={{ textAlign: 'center', fontSize: 20 }}>Register</Text>
                        <View style={{ marginTop: 20,flex: 1 }}>
                            <View style={{ borderColor: 'grey', marginTop: 20,borderWidth: 1, borderRadius: 5}}>
                                <TextInput placeholder="Enter name here" onChangeText={this._setName}/>
                            </View>
                            <View style={{ borderColor: 'grey',marginTop: 20, borderWidth: 1, borderRadius: 5}}>
                                <TextInput placeholder="Enter email here" onChangeText={this._setEmail}/>
                            </View>
                            <View style={{ borderColor: 'grey',marginTop: 20, borderWidth: 1, borderRadius: 5}}>
                                <TextInput placeholder="Enter password here" secureTextEntry onChangeText={this._setPassword}/>
                            </View>
                            <View style={{ borderColor: 'grey',marginTop: 20, borderWidth: 1, borderRadius: 5}}>
                                <TextInput placeholder="Confirm password here" secureTextEntry onChangeText={this._setConfirmPassword}/>
                            </View>
                            <TouchableNativeFeedback onPress={this._register}>
                                <View style={{ width: '100%', marginTop: 30, backgroundColor:'skyblue', padding: 10, borderRadius:5 }}>
                                    <Text style={{ color: 'white', textAlign: 'center' }}>Register</Text>
                                </View>
                            </TouchableNativeFeedback>
                        </View>
                    </View>
                </View>
                {this._renderDock()}
            </View>
        )
    }
}