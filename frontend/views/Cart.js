import React, { Component } from "react";
import { View, TextInput, Modal, Text, FlatList, ToastAndroid, TouchableNativeFeedback, Image, Button } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome5';
import axios from '../utils/axios';
import CheckBox from 'react-native-check-box';
import _ from 'lodash';

export default class Cart extends Component{
    state = {
        carts: [],
        modal: false,
        address: '',
        payment_mode: null,
        isChecked: false,
        errors: {}
    }
    componentDidMount(){
        this._fetchCart();
    }
    _submit = async() => {
        try {
            let items = this.state.carts.map((item) => {
                return {
                    item_id: item.item_id,
                    quantity: item.quantity,
                    sub_total: item.sub_total
                }
            })
            let data = {
                items,
                address: this.state.address,
                total: _.sum(this.state.carts.map(cart => {
                    return cart.sub_total
                })),
                payment_mode: this.state.isChecked ? 'cod': null
            }
            
            let api_token = await AsyncStorage.getItem('api_token');
            let response = await axios({
                method: 'POST',
                url:'/api/orders',
                data,
                headers:{
                    'Authorization': 'Bearer '+api_token
                }
            })
            this.setState({modal: false});
            ToastAndroid.showWithGravity(response.data.message, ToastAndroid.LONG, ToastAndroid.CENTER)
            this._fetchCart();
        } catch (error) {
            if(error.hasOwnProperty('response')){
                this.setState({errors: error.response.data.errors});
                return ToastAndroid.showWithGravity(error.response.data.message, ToastAndroid.SHORT, ToastAndroid.BOTTOM)
            }
            return ToastAndroid.showWithGravity(error.message, ToastAndroid.SHORT, ToastAndroid.BOTTOM)
        }
    }
    _removeCartItem = async(id) => {
        try {
            let api_token = await AsyncStorage.getItem('api_token');

            let response = await axios({
                method: 'delete',
                url: '/api/carts/'+id,
                headers:{
                    'Authorization': 'Bearer '+api_token
                }
            })
            this._fetchCart();
        } catch (error) {
            if(error.hasOwnProperty('response')){
                ToastAndroid.showWithGravity(error.response.data.message, ToastAndroid.CENTER, ToastAndroid.BOTTOM)
            }
            ToastAndroid.showWithGravity(error, ToastAndroid.CENTER, ToastAndroid.BOTTOM)
        }
    }
    _setAddress = (value) => {
        this.setState({address: value});
    }
    _fetchCart = async () => {
        try {
            let api_token = await AsyncStorage.getItem('api_token');

            let response = await axios({
                method: 'GET',
                url: '/api/carts',
                headers:{
                    'Authorization': 'Bearer '+ api_token
                }
            })
            
            this.setState({carts: response.data});
        } catch (error) {
            if(error.hasOwnProperty('response')){
                return ToastAndroid.showWithGravity(error.response.data.message, ToastAndroid.BOTTOM, ToastAndroid.LONG)
            }
            ToastAndroid.showWithGravity('Error Occured', ToastAndroid.BOTTOM, ToastAndroid.LONG)
        }
    }

    _renderPlaceOrderButton = () => {
        if(this.state.carts.length){
            return (<Button title="Place Order" onPress={() => this.setState({modal: true})}/>)
        }
        return null
    }

    render(){
        return(
            <View style={{ flex:1 }}>
                <Modal animationType="slide" visible={this.state.modal} transparent={true}>
                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}>
                        <View style={{ width: '80%', height: 'auto', borderRadius: 5, backgroundColor: 'white' }}>
                            <View style={{ padding: 10 }}>
                                <View style={{ borderWidth:1, borderRadius: 5, borderColor: 'lightgrey' }}>
                                    <TextInput placeholder="Enter your address" onChangeText={this._setAddress} />
                                </View>
                                <View style={{ paddingVertical: 10 }}>
                                    <CheckBox
                                        style={{ flex: 1, padding: 10}}
                                        onClick={()=>{
                                            this.setState({
                                                isChecked:!this.state.isChecked
                                            })
                                        }}
                                        isChecked={this.state.isChecked}
                                        leftText={"Cash On Delivery"}
                                    />
                                    <Text style={{ color: 'red' }}>{this.state.errors.payment_mode}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ backgroundColor: 'white', width: '60%',marginTop: 20, justifyContent: 'space-around', flexDirection:'row', borderRadius: 5 }}>
                            <TouchableNativeFeedback onPress={()=> this.setState({modal: false})}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', flex:1, padding: 10, borderWidth: 0.3, borderColor: 'lightgrey', borderRadius: 5}}>
                                    <Text>Cancel</Text>
                                </View>
                            </TouchableNativeFeedback>
                            <TouchableNativeFeedback onPress={this._submit}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', flex:1, padding: 10, borderWidth: 0.3, borderColor: 'lightgrey', borderRadius: 5}}>
                                    <Text>Order</Text>
                                </View>
                            </TouchableNativeFeedback>
                        </View>
                    </View>
                </Modal>
                <FlatList data={this.state.carts}  keyExtractor = {(item) => item.id.toString()} renderItem={({item}) => (
                    <View style={{ height: 300, backgroundColor: 'white', margin: 10, elevation: 5, borderRadius: 5 }}>
                        <Image source={{ uri: axios.defaults.baseURL+item.item.image }} style={{ flex:2, width: '100%',borderTopLeftRadius: 5, borderTopRightRadius: 5 }}/>
                        <View style={{ padding: 10, flex:1 }}>
                            <View style={{ flex:1, flexDirection:'row', justifyContent: 'space-between'}}>
                                <View>
                                    <Text style={{ fontSize: 20 }}>Name: {item.item.name}</Text>
                                </View>
                                <View style={{ flex:1,alignItems: 'flex-end', flexDirection:'column', justifyContent: 'flex-start'}}>
                                    <Text style={{ fontSize: 15 }}>Price: {item.item.price} Rs.</Text>
                                    <Text style={{ fontSize: 15 }}>Quantity: {item.quantity}</Text>
                                    <Text style={{ fontSize: 15 }}>Sub total: {item.sub_total} Rs.</Text>
                                </View>
                            </View>
                        </View>
                        <TouchableNativeFeedback onPress={()=> {this._removeCartItem(item.id)}}>
                            <View style={{ backgroundColor:'red', borderBottomLeftRadius: 5, borderBottomRightRadius: 5, padding: 5,position: 'absolute',bottom: 0,right: 0, width: '100%' }}>
                                <Text style={{ color: 'white', textAlign: 'center', paddingVertical: 2 }}><Icon name="trash" size={10}/> Remove</Text>
                            </View>
                        </TouchableNativeFeedback>
                    </View>
                )}/>
                {this._renderPlaceOrderButton()}
            </View>
        )
    }
}
