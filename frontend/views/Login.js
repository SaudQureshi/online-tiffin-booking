import React,{ Component } from 'react';
import { Keyboard, View, Text, TextInput, TouchableOpacity, TouchableNativeFeedback, ToastAndroid } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import axios from '../utils/axios';

export default class Login extends Component{
    state = {
        keyboardState: 'closed',
        email: '',
        password: '',
        errors: {}
    }

    componentDidMount(){
        this._checkIfLoggedIn();
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }
    componentWillUnmount(){
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    _checkIfLoggedIn = async () => {
        try {
            let api_token = await AsyncStorage.getItem('api_token');
            if(api_token){
                this.props.navigation.navigate('TabNavigatorContainer')
            }
        } catch (error) {
            ToastAndroid.showWithGravity(error, ToastAndroid.BOTTOM, ToastAndroid.CENTER)
        }
    }
    _keyboardDidShow = () => {
        this.setState({
            keyboardState: 'opened'
        });
      }
    
    _keyboardDidHide = () => {
        this.setState({
            keyboardState: 'closed'
        });
    }
    _renderDock = () =>{
        if(this.state.keyboardState =='closed'){
            return (
                <View style={{ position: 'absolute', bottom: 0, padding:10, width: '100%', backgroundColor:'orange' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
                        <View>
                            <Text style={{ textAlign: 'center', color: 'white' }}>Register</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            )
        }
        return null;
    }
    _setEmail = (value) => {
        this.setState({email: value})
    }
    _setPassword = (value) => {
        this.setState({password: value})
    }
    _login = async () => {
        try {
            let response = await axios({
                method:'POST',
                url: '/api/auth/login',
                data: {
                    email: this.state.email,
                    password: this.state.password
                }
            })
            await AsyncStorage.setItem('api_token', response.data.api_token);
            this.props.navigation.navigate('MainTabNavigator')
        } catch (error) {
            if(error.hasOwnProperty('response')){
                this.setState({errors: error.response.data.errors});
                return ToastAndroid.showWithGravity(error.response.data.message, ToastAndroid.LONG, ToastAndroid.BOTTOM);
            }
            ToastAndroid.showWithGravity('Cannot Login', ToastAndroid.SHORT, ToastAndroid.BOTTOM);
        }
        
    }
    render(){
        return(
            <View style={{ flex:1, flexDirection: 'column' }}>
                <View style={{ flex:1, flexDirection: 'row', justifyContent: 'center', marginTop: '30%'}}>
                    <View style={{ flex:1, paddingHorizontal: 10 }}>
                        <Text style={{ textAlign: 'center', fontSize: 20 }}>Login</Text>
                        <View style={{ marginTop: 20,flex: 1 }}>
                            <View style={{ borderColor: 'grey', marginTop: 20,borderWidth: 1, borderRadius: 5}}>
                                <TextInput placeholder="Enter email here" keyboardType="email-address" onChangeText={this._setEmail}/>
                            </View>
                            <Text style={{ color: 'red' }}>{this.state.errors.email}</Text>
                            <View style={{ borderColor: 'grey',marginTop: 20, borderWidth: 1, borderRadius: 5}}>
                                <TextInput placeholder="Enter password here" secureTextEntry onChangeText={this._setPassword}/>
                            </View>
                            <Text style={{ color: 'red' }}>{this.state.errors.password}</Text>
                            <TouchableNativeFeedback onPress={this._login}>
                                <View style={{ width: '100%', marginTop: 30, backgroundColor:'skyblue', padding: 10, borderRadius:5 }}>
                                    <Text style={{ color: 'white', textAlign: 'center' }}>Login</Text>
                                </View>
                            </TouchableNativeFeedback>
                        </View>
                    </View>
                </View>
                {this._renderDock()}
            </View>
        )
    }
}
