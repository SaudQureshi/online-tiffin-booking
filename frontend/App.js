import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {createAppContainer, createSwitchNavigator, createBottomTabNavigator, createStackNavigator} from 'react-navigation';
import Login from './views/Login';
import Cart from './views/Cart';
import Register from './views/Register';
import Items from './views/Items';
import Item from './views/Item';
import Orders from './views/Orders';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class App extends Component{
  render() {
    return (
      <View style={{ flex:1 }}>
        <AppContainer />
      </View>
    );
  }
}

logOut = async (navigation) => {
  await AsyncStorage.removeItem('api_token');
  navigation.navigate('Login');
}

const MainTabNavigator = createBottomTabNavigator({
  Items: {screen:Items,
  navigationOptions: () => ({
    tabBarIcon: ({tintColor}) => (<Icon name="hamburger" size={20} color={tintColor} tintColor={'orange'}/>)
  })},
  Orders: {screen: Orders,
    navigationOptions: () => ({
      tabBarIcon: ({tintColor}) => (<Icon name="clipboard-list" size={20} color={tintColor} tintColor={'orange'}/>)
    })},
})

const TabNavigatorContainer = createStackNavigator({
  MainTabNavigator,
  Item,
  Cart
}, {
  defaultNavigationOptions: ({navigation}) => {
    return {
      headerTitle: <Text style={{ textAlign: 'center', fontSize: 25, paddingLeft: 10 }}>Foodie</Text>,
      headerRight: (
        <View style={{ flex:1, flexDirection: 'row' }}>
          <TouchableOpacity onPress={() => navigation.navigate('Cart')}>
            <View style={{ paddingHorizontal: 10 }}>
              <Icon name="cart-plus"/>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => logOut(navigation)}>
            <View style={{ paddingHorizontal: 10 }}>
              <Icon name="user-lock"/>
            </View>
          </TouchableOpacity>
        </View>
      )
    }
  }
})

const AppContainer = createAppContainer(
  createSwitchNavigator({
      Login,
      Register,
      TabNavigatorContainer
  }, {
      initialRouteName: 'Login'
  })
)
